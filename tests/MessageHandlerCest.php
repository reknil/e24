<?php

namespace tests;

use E24\Handler\TelegramMessenger;
use E24\Message\Payload;
use Tests\Support\FunctionalTester;

class HandlerCest
{
    public function testTG(FunctionalTester $I)
    {
        $message = TelegramMessenger::buildPayloadFromSomething('body');
        $I->assertInstanceOf(Payload::class, $message);
        $I->assertEquals('telegram', $message->getType());
        $I->assertEquals('body', $message->getMessage());

//        /** @var MessageHandler $messageHandler */
//        $messageHandler = $I->grabService(MessageHandler::class);
//        $messageHandler->handle(TelegramMessenger::buildPayloadFromSomething('body'));
        //TODO Check bus or etc.
    }

}
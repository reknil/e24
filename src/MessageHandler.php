<?php

namespace E24;

use E24\Exception\RemoteServiceException;
use E24\Exception\RemoteValidationException;
use E24\Interface\MessageInterface;
use E24\Interface\MessengerInterface;
use Psr\Log\LoggerInterface;

class MessageHandler
{

    /**
     * @var MessengerInterface[]
     */
    private iterable $handlers;

    public function __construct(private readonly LoggerInterface $logger, iterable $handlers)
    {
        $this->handlers = $handlers;
    }


    /**
     * @throws RemoteValidationException
     * @throws RemoteServiceException
     */
    public function handle(MessageInterface $message): void
    {
        foreach ($this->handlers as $handler) {
            if ($handler->isSupport($message->getType())) {
                try {
                    $handler->send($message->getMessage());
                } catch (RemoteServiceException $e) {
                    $this->logger->error('Remote Service Exception', []);

                    throw $e; // TBD
                } catch (RemoteValidationException $e) {
                    $this->logger->info('Remote Validation Exception', []);
                    throw $e; // TBD
                }
            }
        }

    }

}
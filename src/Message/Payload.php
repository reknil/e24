<?php

namespace E24\Message;

use E24\Interface\MessageInterface;

class Payload implements MessageInterface
{
    public function __construct(private string $type, private string $message)
    {
    }

    /**
     * Get type of the message
     *
     * @return string Eg "telegram", "whatsapp", "push"
     * @throws TypeError Unknown type in Payload
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get message text
     *
     * @return string
     * @throws ValueError Could not get a message from Payload
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
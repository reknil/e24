<?php

namespace E24\Interface;

interface MessageInterface
{
    public function getType(): string;

    public function getMessage(): string;

}
<?php

namespace E24\Interface;

use E24\Exception\RemoteServiceException;
use E24\Exception\RemoteValidationException;

interface MessengerInterface
{
    /**
     * @param string $message
     * @return string
     * @throws RemoteServiceException
     * @throws RemoteValidationException
     */
    public function send(string $message): string;

    public function isSupport(string $type): bool;
}

<?php

namespace E24\Handler;

use E24\Interface\MessengerInterface;
use E24\Message\Payload;

class TelegramMessenger implements MessengerInterface
{
    private const TYPE = 'telegram';

    public static function buildPayloadFromSomething(string $message): Payload
    {
        return new Payload(self::TYPE, $message);

    }

    public function send(string $message): string
    {
        return '';
    }

    public function isSupport(string $type): bool
    {
        return self::TYPE === $type;
    }


}